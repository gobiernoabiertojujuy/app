import React from 'react';
import {AppMainNavigation} from './src/app-navigator';
import API from './util/api';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './store';
import Loading from './src/sections/components/loading';

class AppLayout extends React.Component {
  state = {};
  async componentDidMount() {
    const newsList = await API.getNews();
    store.dispatch({
      type: 'SET_NEWS_LIST',
      payload: {newsList},
    });

    const documentList = await API.getDocuments();
    store.dispatch({
      type: 'SET_DOCUMENTS_LIST',
      payload: {documentList},
    });

    const contactList = await API.getContacts();
    store.dispatch({
      type: 'SET_CONTACTS_LIST',
      payload: {contactList},
    });
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <AppMainNavigation />
        </PersistGate>
      </Provider>
    );
  }
}

export default AppLayout;
