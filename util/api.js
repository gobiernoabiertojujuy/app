const url =
  'http://www.gajujuy.gob.ar/wp-content/uploads/solicitudes/service-app.json';

class Api {
  async getCategories() {
    const request = await fetch(url);
    const data = await request.json();
    return data.categorias;
  }

  async getNews() {
    const request = await fetch(url);
    const data = await request.json();
    return data.novedades;
  }

  async getDocuments() {
    const request = await fetch(url);
    const data = await request.json();
    return data.normativas;
  }

  async getDocument(text) {
    const request = await fetch(url);
    const data = await request.json();
    const search = text.toUpperCase();
    var found = [];
    var normativas = data.normativas;
    for (let i = 0; i < data.normativas.length; i++) {
      if (normativas[i].summary.toUpperCase().includes(search)) {
        found.push(normativas[i]);
      }
    }
    return found;
  }

  async getContacts() {
    const request = await fetch(url);
    const data = await request.json();
    return data.contacto;
  }

  async getContact(text) {
    const request = await fetch(url);
    const data = await request.json();
    const search = text.toUpperCase();
    var found = [];
    var contactos = data.contacto;
    for (let i = 0; i < data.contactos.length; i++) {
      if (contactos[i].name.toUpperCase().includes(search)) {
        found.push(contactos[i]);
      }
    }
    return found;
  }

}

export default new Api();
