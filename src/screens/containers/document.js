import React from 'react';
import {StyleSheet, View} from 'react-native';
import DocumentList from '../../documents/containers/document-list';

class Documents extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <DocumentList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
});

export default Documents;
