import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import ContactList from '../../documents/containers/contact-list';

class Phone extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ContactList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
});

export default Phone;
