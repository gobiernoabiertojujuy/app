import React from 'react';
import {View, StyleSheet} from 'react-native';
import Header from '../../sections/components/header';
import NewsList from '../../documents/containers/news-list';

class Main extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <NewsList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 22,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
});

export default Main;
