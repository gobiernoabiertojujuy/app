import React from 'react';

class Phone extends React.Component {
  render() {
    return this.props.children;
  }
}

export default Phone;
