import React from 'react';

class News extends React.Component {
  render() {
    return this.props.children;
  }
}

export default News;
