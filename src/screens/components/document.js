import React from 'react';

class Document extends React.Component {
  render() {
    return this.props.children;
  }
}

export default Document;
