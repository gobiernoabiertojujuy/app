import React, {Component} from 'react';
import {TextInput, StyleSheet} from 'react-native';

import API from '../../../util/api';

import {connect} from 'react-redux';

class Search extends Component {
  state = {
    text: '',
  };
  handleSubmit = async () => {
    console.log('submit');
    const documentList = await API.getDocument(this.state.text);
    console.log(this.state);
    console.log(this.props);
    this.props.dispatch({
      type: 'SET_DOCUMENTS_LIST',
      payload: {documentList},
    });
  };
  handleChangeText = text => {
    this.setState({
      text,
    });
  };
  render() {
    return (
      <TextInput
        placeholder="Buscar"
        autoCorrect={false}
        autoCapitalize="none"
        underlineColorAndroid="transparent"
        onSubmitEditing={this.handleSubmit}
        onChangeText={this.handleChangeText}
        style={styles.input}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    padding: 15,
    fontSize: 15,
    borderWidth: 1,
    borderColor: '#eaeaea',
  },
});

export default connect(null)(Search);
