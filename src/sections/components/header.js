import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Linking,
  StyleSheet,
  SafeAreaView,
} from 'react-native';

function Header(props) {
  return (
    <View>
      <SafeAreaView>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL('http://www.gajujuy.gob.ar/');
          }}>
          <Image
            source={require('../../../assets/logo.png')}
            style={styles.logo}
          />
        </TouchableOpacity>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    height: 120,
    width: 360,
  },
});

export default Header;
