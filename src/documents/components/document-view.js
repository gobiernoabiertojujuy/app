import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function Document({route, navigation}) {
  const {norma, summary, date, type} = route.params;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{JSON.stringify(norma)}</Text>
      <View style={styles.description}>
        <View style={styles.details}>
          <Text style={styles.titleDetails}>Medida: </Text>
          <Text>{JSON.stringify(type)}</Text>
        </View>
        <View style={styles.details}>
          <Text style={styles.titleDetails}>Fecha: </Text>
          <Text>{JSON.stringify(date)}</Text>
        </View>
      </View>
      <Text style={styles.summary}>{JSON.stringify(summary)}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: 0,
    padding: 10,
  },
  description: {
    flexDirection: 'row',
  },
  details: {
    fontSize: 18,
    lineHeight: 22.5,
    margin: 10,
    flex: 1,
  },
  titleDetails: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  title: {
    paddingTop: 20,
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 6,
    backgroundColor: '#85CEE4',
    color: 'white',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  summary: {
    fontSize: 18,
    textAlign: 'justify',
  },
});

export default Document;
