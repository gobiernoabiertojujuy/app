import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';

function News(props) {
  return (
    <TouchableOpacity
      onPress={() => {
        Linking.openURL(props.url);
      }}>
      <View style={styles.container}>
        <Image
          source={require('../../../assets/icons/escudo.png')}
          style={styles.logo}
        />
        <View style={styles.right}>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.summary}>{props.summary}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    margin: 5,
  },
  logo: {
    height: 80,
    width: 110,
  },
  right: {
    width: 300,
    minHeight: 50,
    paddingLeft: 5,
    alignSelf: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: '#44546b',
  },
  type: {
    backgroundColor: '#3B4395',
    paddingVertical: 8,
    paddingHorizontal: 12,
    color: 'white',
    fontSize: 14,
    borderRadius: 5,
    overflow: 'hidden',
    alignSelf: 'flex-start',
  },
  summary: {
    color: '#6b6b6b',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default News;
