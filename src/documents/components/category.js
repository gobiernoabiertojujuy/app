import React from 'react';
import {Text, ImageBackground, StyleSheet} from 'react-native';

function Category(props) {
  return (
    <ImageBackground
      style={styles.wrapper}
      source={require('../../../assets/backgronund.png')}> 
      <Text style={styles.genre}>{props.type}</Text>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    height: 'auto',
    borderRadius: 10,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  genre: {
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold',
    textShadowOffset: {
      width: 10,
      height: 10,
    },
    textShadowRadius: 0,
  },
});

export default Category;
