import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Platform,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faAddressCard} from '@fortawesome/free-solid-svg-icons';
import {useNavigation} from '@react-navigation/native';

function Contact(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.cover}>
        <FontAwesomeIcon icon={faAddressCard} color={'#1DAF8E'} size={100} />
      </View>

      <View style={styles.right}>
        <Text style={styles.title}>{props.name}</Text>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(props.web);
          }}>
          <Text style={styles.summary}>Sitio Web</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (Platform.OS === 'android') {
              Linking.openURL('tel:' + props.phones[0]);
            } else {
              Linking.openURL('telprompt:' + props.phones[0]);
            }
          }}>
          <Text style={styles.summary}>Llamar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Contacto', {
              title: props.name,
              name: props.name,
              phones: props.phones,
              web: props.web,
              organigrama: props.organigrama,
              autoridades: props.autoridades,
              location: props.location,
              locationX: props.locationX,
              locationY: props.locationY,
            });
          }}>
          <Text style={styles.type}>Ver más</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  cover: {
    resizeMode: 'contain',
    backgroundColor: '#ffff',
  },
  logo: {
    height: 100,
    width: 100,
  },
  right: {
    minHeight: 130,
    marginLeft: 10,
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 24,
    textTransform: 'uppercase',
    color: '#2E2D2C',
  },
  type: {
    backgroundColor: '#1DAF8E',
    paddingVertical: 4,
    paddingHorizontal: 6,
    color: 'white',
    fontSize: 14,
    borderRadius: 5,
    overflow: 'hidden',
    alignSelf: 'flex-start',
  },
  summary: {
    color: '#6b6b6b',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Contact;
