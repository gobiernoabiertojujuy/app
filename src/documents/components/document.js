import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faFileAlt} from '@fortawesome/free-solid-svg-icons';
import {useNavigation} from '@react-navigation/native';

function Suggestion(props) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        console.log(props);
        navigation.navigate('Documento', {
          screen: 'Normativa',
          norma: props.title,
          summary: props.summary,
          date: props.date,
          type: props.type,
          url: props.url,
        });
      }}>
      <View style={styles.container}>
        <View style={styles.cover}>
          <FontAwesomeIcon icon={faFileAlt} color={'#85CEE4'} size={100} />
        </View>
        <View style={styles.right}>
          <Text style={styles.title}>{props.title}</Text>
          <View style={styles.button}>
            <View style={styles.genre}>
              <Text style={styles.genreText}>Normativa {props.type}</Text>
            </View>
            <View style={styles.date}>
              <Text style={styles.genreText}>Publicada: {props.date}</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  genre: {
    width: 120,
    backgroundColor: '#8A8A8D',
    paddingVertical: 5,
    paddingHorizontal: 7,
  },
  date: {
    width: 120,
    backgroundColor: '#85CEE4',
    paddingVertical: 5,
    paddingHorizontal: 7,
  },
  genreText: {
    color: '#ffff',
    fontStyle: 'normal',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 14,
  },
  cover: {
    resizeMode: 'contain',
    backgroundColor: '#ffff',
  },
  right: {
    minHeight: 120,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 24,
    textTransform: 'uppercase',
    color: '#44546b',
  },
  summary: {
    flexDirection: 'row',
    color: '#6b6b6b',
    alignItems: 'stretch',
    alignContent: 'stretch',
  },
  description: {
    fontSize: 18,
    lineHeight: 22.5,
    margin: 10,
    alignContent: 'flex-start',
    textAlign: 'left',
    flex: 1,
  },
  button: {
    flexDirection: 'row',
  },
});

export default Suggestion;
