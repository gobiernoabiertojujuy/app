import React from 'react';
import {View, StyleSheet} from 'react-native';

function DocumentListLayout(props) {
  return <View style={styles.container}>{props.children}</View>;
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    flex: 1,
  },
});

export default DocumentListLayout;
