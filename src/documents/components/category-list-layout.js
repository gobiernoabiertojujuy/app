import React from 'react';
import {Text, StyleSheet, ImageBackground} from 'react-native';

function CategoryListLayout(props) {
  return (
    <ImageBackground
      source={require('../../../assets/backgronund.png')}
      style={styles.container}>
      <Text style={styles.title}>{props.title}</Text>
      {props.children}
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: '#85CEE4',
  },
  title: {
    borderWidth: 5,
    borderColor: 'red ',
    color: '#4c4c4c',
    fontSize: 20,
    marginBottom: 10,
    padding: 5,
    margin: 10,
    fontWeight: 'bold',
  },
});

export default CategoryListLayout;
