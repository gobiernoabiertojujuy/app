import React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Platform,
} from 'react-native';

function ContactDetail({route, navigation}) {
  const {name, phones, web, location, autoridades, organigrama} = route.params;

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.title}>{JSON.stringify(name)}</Text>
        <View style={styles.details}>
          <Text style={styles.description}>Sitio Web: </Text>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(web);
            }}>
            <Text style={styles.description}>{JSON.stringify(web)}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.details}>
          <Text style={styles.description}>Teléfonos:</Text>
          <TouchableOpacity
            onPress={() => {
              if (Platform.OS === 'android') {
                Linking.openURL('tel:' + phones[0]);
              } else {
                Linking.openURL('telprompt:' + phones[0]);
              }
            }}>
            <Text style={styles.description}>{JSON.stringify(phones[0])}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.details}>
          <Text style={styles.description}>Dirección:</Text>
          <Text style={styles.description}>{JSON.stringify(location)}</Text>
        </View>
        <View style={styles.button}>
          <View style={styles.description}>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(autoridades);
              }}>
              <Text style={styles.type}>Ver Autoridades</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.description}>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(organigrama);
              }}>
              <Text style={styles.type}>Ver Organigrama</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: 0,
    padding: 10,
  },
  details: {
    flexDirection: 'row',
  },
  description: {
    fontSize: 18,
    lineHeight: 22.5,
    margin: 10,
    paddingTop: 20,
    alignContent: 'flex-start',
    textAlign: 'left',
    flex: 1,
  },
  title: {
    paddingTop: 20,
    marginTop: 5,
    paddingVertical: 8,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 6,
    backgroundColor: '#1DAF8E',
    color: 'white',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  type: {
    paddingVertical: 8,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 6,
    backgroundColor: '#1DAF8E',
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  summary: {
    fontSize: 18,
    alignContent: 'flex-start',
  },
  button: {
    flexDirection: 'row',
  },
});

export default ContactDetail;
