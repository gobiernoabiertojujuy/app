import React, {Component} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import Layout from '../../../src/documents/components/document-list-layout';
import Empty from '../../../src/documents/components/empty';
import Separator from '../../../src/documents/components/vertical-separator';
import Element from '../../../src/documents/components/document';
import Search from '../../sections/container/searchDocuments';
import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {
    list: state.documents.documentList,
  };
}

class DocumentList extends Component {
  keyExtractor = item => item.id.toString();
  renderEmtpy = () => <Empty text="No hay normativas" type="documento" />;
  itemSeparator = () => <Separator />;
  renderItemElement = ({item}) => {
    return <Element {...item} />;
  };
  render() {
    return (
      <Layout style={styles.container}>
        <Search />
        <FlatList
          style={styles.options}
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmtpy}
          ItemSeparatorComponent={this.itemSeparator}
          renderItem={this.renderItemElement}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingRight: 20,
    paddingTop: 22,
    alignItems: 'center',
    justifyContent: 'center',
  },
  options: {
    paddingRight: 20,
    paddingHorizontal: 20,
    textAlign: 'left',
    alignContent: 'stretch',
  },
});

export default connect(mapStateToProps)(DocumentList);
