import React, {Component} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import Layout from '../../documents/components/news-list-layout';
import Empty from '../../documents/components/empty';
import Separator from '../../documents/components/vertical-separator';
import Element from '../../documents/components/news';

import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {
    list: state.news.newsList,
  };
}

class NewsList extends Component {
  keyExtractor = item => item.id.toString();
  renderEmtpy = () => <Empty text="No hay Novedades" />;
  itemSeparator = () => <Separator />;
  renderItemElement = ({item}) => {
    return <Element {...item} />;
  };
  render() {
    return (
      <Layout title="Enlaces de Interes" style={styles.container}>
        <FlatList
          style={styles.options}
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmtpy}
          ItemSeparatorComponent={this.itemSeparator}
          renderItem={this.renderItemElement}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 22,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
  options: {
    paddingHorizontal: 20,
  },
});

export default connect(mapStateToProps)(NewsList);
