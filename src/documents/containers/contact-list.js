import React, {Component} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import Layout from '../../../src/documents/components/contact-list-layout';
import Empty from '../components/empty';
import Separator from '../components/vertical-separator';
import Element from '../../../src/documents/components/contacto';
import Search from '../../sections/container/searchContacts';
import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {
    list: state.contacts.contactList,
  };
}

class ContactList extends Component {
  keyExtractor = item => item.id.toString();
  renderEmtpy = () => <Empty text="No hay contactos" type="contacto" />;
  itemSeparator = () => <Separator />;
  renderItemElement = ({item}) => {
    return <Element {...item} />;
  };
  render() {
    return (
      <Layout style={styles.container}>
        <Search />
        <FlatList
          style={styles.options}
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmtpy}
          ItemSeparatorComponent={this.itemSeparator}
          renderItem={this.renderItemElement}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingRight: 20,
    paddingTop: 22,
    alignItems: 'center',
    justifyContent: 'center',
  },
  options: {
    paddingRight: 20,
    paddingHorizontal: 20,
    textAlign: 'left',
    alignContent: 'stretch',
  },
});

export default connect(mapStateToProps)(ContactList);
