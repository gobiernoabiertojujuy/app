import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHome, faFile, faAddressBook} from '@fortawesome/free-solid-svg-icons';

import HomeScreen from './screens/containers/home';
import DocumentScreen from './screens/containers/document';
import PhoneScreen from './screens/containers/phone';

import DocumentViewScreen from '../src/documents/components/document-view';
import ContactViewScreen from '../src/documents/components/contact-view';

const Tab = createBottomTabNavigator();

function AppMainNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          backgroundColor: '#dff2fd',
          activeTintColor: '#3B4395',
          inactiveTintColor: '#2E2D2C',
          showIcon: true,
        }}>
        <Tab.Screen
          name="home"
          component={HomeScreen}
          options={{
            title: 'Inicio',
            tabBarIcon: ({color}) => (
              <FontAwesomeIcon icon={faHome} color={color} size={30} />
            ),
          }}
        />
        <Tab.Screen
          name="news"
          component={DocumentNavegation}
          options={{
            title: 'Documentos',
            tabBarIcon: ({color}) => (
              <FontAwesomeIcon icon={faFile} color={color} size={30} />
            ),
          }}
        />
        <Tab.Screen
          name="phone"
          component={ContactoNavegation}
          options={{
            title: 'Teléfonos',
            tabBarIcon: ({color}) => (
              <FontAwesomeIcon icon={faAddressBook} color={color} size={30} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const Stack = createStackNavigator();

function DocumentNavegation() {
  return (
    <Stack.Navigator
      initialRouteName="Normativa"
      headerMode="screen"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: {backgroundColor: '#85CEE4'},
      }}>
      <Stack.Screen
        name="Normativas"
        component={DocumentScreen}
        options={{
          title: 'Normativa',
          gestureEnabled: true,
        }}
      />
      <Stack.Screen
        name="Documento"
        component={DocumentViewScreen}
        options={{
          title: 'Documento',
          gestureEnabled: true,
        }}
      />
    </Stack.Navigator>
  );
}

function ContactoNavegation() {
  return (
    <Stack.Navigator
      initialRouteName="Phone"
      headerMode="screen"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: {backgroundColor: '#1DAF8E'},
      }}>
      <Stack.Screen
        name="Phone"
        component={PhoneScreen}
        options={{title: 'Teléfonos'}}
      />
      <Stack.Screen
        name="Contacto"
        component={ContactViewScreen}
        options={{title: 'Contacto'}}
      />
    </Stack.Navigator>
  );
}

export {AppMainNavigation, DocumentNavegation, ContactoNavegation};
