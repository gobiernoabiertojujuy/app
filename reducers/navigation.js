import {createNavigationReducer} from 'react-navigation-redux-helpers';
import AppNavigation from '../src/app-navigator';

const navigationReducer = createNavigationReducer(AppNavigation);

export default navigationReducer;
