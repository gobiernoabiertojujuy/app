function contacts(state = {}, action) {
  switch (action.type) {
    case 'SET_CONTACTS_LIST': {
      return {...state, ...action.payload};
    }
    default:
      return state;
  }
}

export default contacts;
