function documents(state = {}, action) {
  switch (action.type) {
    case 'SET_DOCUMENTS_LIST': {
      return {...state, ...action.payload};
    }
    case 'SEARCH_DOCUMENTS_LIST': {
      return {...state, ...action.payload};
    }
    default:
      return state;
  }
}

export default documents;
