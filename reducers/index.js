import {combineReducers} from 'redux';
import documents from './documents';
import news from './news';
import contacts from './contacts';

export default combineReducers({documents, news, contacts});
