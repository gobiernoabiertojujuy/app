# [Dirección de Transparencia y Gobierno Abierto](http://www.gajujuy.gob.ar/ "Heading link")
---

## Documentación

Enlaces a la documentación oficial de para consultar las configuraciones, métodos, funciones utilizadas en el proyecto.

> [Documentación de NodeJS](https://nodejs.org/es/ "link")

> [Documentación de ReactJS](https://es.reactjs.org/ "link")

> [Documentación de React Native](https://reactnative.dev/ "link")

> [Documentación de React Navigation](https://reactnavigation.org/ "link")

> [Documentación de Redux](https://redux.js.org/basics/usage-with-react "link")

> [Documentación de Android](https://developer.android.com/ "link")

## Entorno

**Previo a la ejecución de la apliación se debió haber realizado las sientes tareas:**
- Instalar y crear las variables de entorno necesarias para el [SDK Manager](https://developer.android.com/studio/releases/platform-tools "link") y crado un dispositivo virtual android
- Instalación de NodeJS
- Instalación de un editor de texto a elección

## Instalación y Ejecución

#### Instalar dependencia del proyecto
Para instalar las dependencias del proyecto debemos abrir una terminal y nos ubicamos dentro del directorio del proyecto y ejecutamos el siguiente comando para instalar las dependencias de declaradas en el archivo [<b>package.json</b>](https://nodejs.dev/the-package-json-guide "link")

    npm i

#### Instala y Ejecuta la aplicación dentro de un emulador android
Para ejecutar el proyecto es necesario tener previamente creado un dispositivo virtual en el cual se instalará y ejecutará la aplicación.

    npx react-native run-android
